import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';

@Component({
  selector: 'app-authform',
  templateUrl: './authform.component.html',
  styleUrls: ['./authform.component.css']
})
export class AuthformComponent implements OnInit {
  authform: FormGroup;
  private isRemember: boolean;

  constructor() { 
    this.authform = new FormGroup({
      usernameControl: new FormControl(null, [Validators.pattern("^[a-zA-Z0-9._]+$"), Validators.minLength(2), Validators.maxLength(30)]),
      passwordControl: new FormControl(null, [Validators.pattern("^[a-zA-Z0-9._]+$"), Validators.minLength(2), Validators.maxLength(30)]),
      remember: new FormControl()
    });
  }

  ngOnInit() {
    this.authform.get("remember").valueChanges.subscribe((value) => {
      if (value === true) {
        this.isRemember = true;
      } else {
        this.isRemember = false;
      }
    });
  }

  login() {
    if (this.isRemember != true) {
      this.isRemember = false;
    }

    $.ajax({
      url: 'http://192.168.2.89:8082/schulupdate-efb759645a77b127496a497bdfe88f363d8cd24c/api/session/login', //url should be here
      type: 'post',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      },
      data: JSON.stringify({
        apiLevel: "3",
        password: this.authform.get("passwordControl").value,
        platform: "webapp", 
        platform_version: "1.1",
        rememberMe: String(this.isRemember),
        username: this.authform.get("usernameControl").value,
        xsrfProtect: "true" 
      }),
      success: function(response) {
        console.log(response);
        
      }
  });
  }

  register() {

  }

}
